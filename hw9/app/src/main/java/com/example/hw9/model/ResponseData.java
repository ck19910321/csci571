package com.example.hw9.model;

import java.util.List;

/**
 * Created by 邑安 on 2016/4/18.
 */
public class ResponseData {

    private DBean d;

    public DBean getD() {
        return d;
    }

    public void setD(DBean d) {
        this.d = d;
    }

    public static class DBean {
        /**
         * __metadata : {"uri":"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/News?Query=u0027aaplu0027&$skip=0&$top=1","type":"NewsResult"}
         * ID : 2f6eeb64-e7fe-4285-8a11-fe064f0d4e86
         * Title : AAPL, NFLX and PBR.A stock got the new trading week started on the wrong foot
         * Url : http://investorplace.com/2016/04/why-apple-inc-aapl-netflix-inc-nflx-and-petroleo-brasileiro-sa-petrobras-adr-pbr-are-3-of-todays-worst-stocks/
         * Source : Investor Place
         * Description : Apple was the subject of a number of headlines on Monday. A report by market intelligence provider TrendForce suggests that iPhone shipments for the first quarter may have fallen nearly 45% year-over-year. The blame was put on iPhone 6s, as u201cthe model ...
         * Date : 2016-04-18T21:05:13Z
         */

        private List<ResultsBean> results;

        public List<ResultsBean> getResults() {
            return results;
        }

        public void setResults(List<ResultsBean> results) {
            this.results = results;
        }

        public static class ResultsBean {
            /**
             * uri : https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/News?Query=u0027aaplu0027&$skip=0&$top=1
             * type : NewsResult
             */

            private MetadataBean __metadata;
            private String ID;
            private String Title;
            private String Url;
            private String Source;
            private String Description;
            private String Date;

            public MetadataBean get__metadata() {
                return __metadata;
            }

            public void set__metadata(MetadataBean __metadata) {
                this.__metadata = __metadata;
            }

            public String getID() {
                return ID;
            }

            public void setID(String ID) {
                this.ID = ID;
            }

            public String getTitle() {
                return Title;
            }

            public void setTitle(String Title) {
                this.Title = Title;
            }

            public String getUrl() {
                return Url;
            }

            public void setUrl(String Url) {
                this.Url = Url;
            }

            public String getSource() {
                return Source;
            }

            public void setSource(String Source) {
                this.Source = Source;
            }

            public String getDescription() {
                return Description;
            }

            public void setDescription(String Description) {
                this.Description = Description;
            }

            public String getDate() {
                return Date;
            }

            public void setDate(String Date) {
                this.Date = Date;
            }

            public static class MetadataBean {
                private String uri;
                private String type;

                public String getUri() {
                    return uri;
                }

                public void setUri(String uri) {
                    this.uri = uri;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }
            }
        }
    }
}