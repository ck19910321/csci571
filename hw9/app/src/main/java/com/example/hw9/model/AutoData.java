package com.example.hw9.model;

/**
 * Created by 邑安 on 2016/4/22.
 */
public class AutoData {

    /**
     * label : AAPL- Apple Inc(NASDAQ)
     * value : AAPL
     */

    private String label;
    private String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
