package com.example.hw9;

/**
 * Created by 邑安 on 2016/4/19.
 */
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AutoListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] item1;
    private final String[] item2;
    //private final Integer[] imgid;

    public AutoListAdapter(Activity context, String[] item1, String[] item2) {
        super(context, R.layout.mylist, item1);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.item1=item1;
        this.item2=item2;
        //this.imgid=imgid;
    }

    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.auto_list, null,true);

        TextView txtValue = (TextView) rowView.findViewById(R.id.item);
        TextView txtLabel = (TextView) rowView.findViewById(R.id.textView1);

        txtValue.setText(item1[position]);
        txtLabel.setText(item2[position]);


        return rowView;

    };
}
