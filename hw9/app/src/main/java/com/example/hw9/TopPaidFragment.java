package com.example.hw9;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.hw9.model.ResponseData;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class TopPaidFragment extends ListFragment {
    private JSONObject JsonSymbol;
    private String JsonNews;
    private TextView textView;
    JSONArray arrNews;
    private JSONObject item;
    public TopPaidFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_top_paid, container, false);
        textView = (TextView) view.findViewById(R.id.text1);
        return view;
    }
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ResultActivity resultActivity = (ResultActivity) getActivity();
        JsonSymbol = resultActivity.getJsonSymbol();
        JsonNews = resultActivity.getJsonNews();
        /*try {
            JSONObject jsNews = new JSONObject(JsonNews);
            System.out.println("entire news: " + jsNews.toString());
            String news = jsNews.getString("d");
            jsNews = new JSONObject(news);
            System.out.println("second  news: " + jsNews.toString());
            news = jsNews.getString("results");
            System.out.println("third  news: " + news);
            arrNews = new JSONArray(news);
            System.out.println("json array length: " + arrNews.length());
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        //System.out.println("news" + JsonNews);
        Gson gson = new Gson();
        //String trimmed = JsonNews.trim();
        JsonReader reader = new JsonReader(new StringReader(JsonNews));
        reader.setLenient(true);
        System.out.println(JsonNews);
        try {
            JSONObject json = new JSONObject(JsonNews);
        } catch (JSONException e) {
            e.printStackTrace();
        }
       ResponseData responseData = gson.fromJson(reader, ResponseData.class);

        final List<String []> stockList = new LinkedList<String []>();

        int length = responseData.getD().getResults().size();
        //System.out.println(length);
        for (int i = 0; i<length;i++){
            String dateStr = responseData.getD().getResults().get(i).getDate();
            stockList.add(new String[] {responseData.getD().getResults().get(i).getUrl(), responseData.getD().getResults().get(i).getTitle(), responseData.getD().getResults().get(i).getDescription()
                    + "\n\n" + "Publisher : " + responseData.getD().getResults().get(i).getSource()
                    + "\n\n" + "Date : " + dateFormat(dateStr)});
        }
        /*final List<String []> stockList = new LinkedList<String []>();

        int length = arrNews.length();
        for (int i = 0; i<length;i++){
            try {
                item = arrNews.getJSONObject(i);
                //item = JsObj.toString();
                stockList.add(new String[] {item.getString("Url"), item.getString("Title"), item.get("Description")
                        + "\n\n" + "Publisher : " + item.getString("Source")
                        + "\n\n" + "Date : " + item.getString("Date")});
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }*/
       ArrayAdapter<String []> arrayAdapter = new ArrayAdapter<String[]>(getListView().getContext(), android.R.layout.simple_list_item_activated_2, android.R.id.text1, stockList ){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position, convertView, parent);
                String [] entry = stockList.get(position);
                TextView textView1 = (TextView) view.findViewById(android.R.id.text1);
                TextView textView2 = (TextView) view.findViewById(android.R.id.text2);
                //String title = "<a href=" + entry[0] + ">" + entry[1];
                //創建一個 SpannableString物件
                SpannableString sp = new SpannableString(entry[1]);
                //設置超連結
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
                sp.setSpan(new URLSpan(entry[0]), 0, entry[1].length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                //設置高亮樣式一
                sp.setSpan(new ForegroundColorSpan(Color.BLACK), 0 ,entry[1].length() ,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                sp.setSpan(bss, 0 ,entry[1].length() ,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                textView1.setText(sp);
                textView1.setTextSize(20);
                //textView1.setText(Html.fromHtml(title));

                textView1.setMovementMethod(LinkMovementMethod.getInstance());


                textView2.setText(entry[2]);

                textView2.setTextSize(14);
                return view;
            }
        };
        getListView().setAdapter(arrayAdapter);
    }
    public String dateFormat (String date){
        date = date.replace("T"," ");
        date = date.replace("Z","");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        Date newDate = null;
        try {
            newDate = format.parse(date);
            System.out.println(newDate.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("dd MMMM yyyy, HH:mm:ss");
        return format.format(newDate);

    }

}
