package com.example.hw9;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends ListFragment {
    private JSONObject JsonSymbol;
    private ImageView imageView;

    ListView list;
    //private List<String[]> stockList = new LinkedList<String[]>();
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        //imageView = (ImageView) view.findViewById(R.id.iv);
        list = (ListView) view.findViewById(android.R.id.list);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //String[] color = {"Red", "Green", "Black", "White", "Blue", "Orange", "Purple", "Yellow"};
        ResultActivity resultActivity = (ResultActivity) getActivity();
        JsonSymbol = resultActivity.getJsonSymbol();
        //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getListView().getContext(), android.R.layout.simple_expandable_list_item_1, color);
        //getListView().setAdapter(arrayAdapter);
        final String[] stock = new String[13];
        final String[] Topic = {"", "NAME", "SYMBOL", "LASTPRICE", "CHANGE", "TIMESTAMP", "MARKETCAP", "VOLUME", "CHANGEYTD", "HIGH", "LOW", "OPEN", "Today's Stock Activity"};
        //final ArrayList<String[]> stockList = new ArrayList<String[] >();
        stock[0] = "";
        Topic[0] = "\n"+ "                            Stock Details                ";
        try {
            stock[1] = JsonSymbol.getString("Name");
            stock[2] = JsonSymbol.getString("Symbol");
            //
            double price = Double.valueOf(JsonSymbol.getString("LastPrice"));
            int tempPrice = (int) Math.round(price * 100);
            double roundPrice = tempPrice / 100.0;
            //String str = String.format("%1.2f", roundPrice);
            //System.out.println(str);
            stock[3] =String.format("%1.2f", roundPrice);
            //
            double Change = Double.valueOf(JsonSymbol.getString("Change"));
            int tempChange = (int) Math.round(Change * 100);
            double roundChange = tempChange / 100.0;
            double ChangeP = Double.valueOf(JsonSymbol.getString("ChangePercent"));
            int tempChangeP = (int) Math.round(ChangeP * 100);
            double roundChangeP = tempChangeP / 100.0;
            stock[4] =String.format("%1.2f", roundChange) + "(" + String.format("%1.2f", roundChangeP) + "%)";
            //
            String dateStr = JsonSymbol.getString("Timestamp");
            dateStr = dateStr.replace("UTC","");
            SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
            Date newDate = null;
            try {
                newDate = format.parse(dateStr);
                System.out.println(newDate.toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            format = new SimpleDateFormat("dd MMMM yyyy, HH:mm:ss");
            String date = format.format(newDate);
            System.out.println(date);
            stock[5] = date;
            //
            double MarCap = Double.valueOf(JsonSymbol.getString("MarketCap"));
            if (MarCap < 1000000) {
                int tempCap = (int) Math.round(MarCap * 100);
                double roundCap = tempCap / 100.0;
                stock[6] = String.format("%1.2f", roundCap);
            } else if (MarCap > 1000000000) {
                //System.out.println(product.getMarketCap());
                double Cap = MarCap / 1000000000.0;
                //System.out.println(String.valueOf(Cap));
                int tempCap = (int) Math.round(Cap * 100);
                //System.out.println(String.valueOf(tempCap));
                double roundCap = tempCap / 100.0;
                //System.out.println(String.valueOf(roundCap));
                stock[6] = String.format("%1.2f", roundCap) + " Billion";
            } else {
                double Cap = MarCap / 1000000.0;
                int tempCap = (int) Math.round(Cap * 100);
                double roundCap = tempCap / 100.0;
                stock[6] = String.format("%1.2f", roundCap) + " Million";
            }

            /*MarCap = MarCap/1000000000.0;
            int tempMarCap = (int) Math.round(MarCap*100);
            double roundMarCap = tempMarCap / 100.0;
            stock[6]=String.valueOf(roundMarCap) + " Billion";*/
            //
            //stock[7] = JsonSymbol.getString("Volume");
            double Vol = Double.valueOf(JsonSymbol.getString("Volume"));
            if (Vol < 1000000) {
                int tempVol = (int) Math.round(Vol * 100);
                double roundVol = tempVol / 100.0;
                stock[7] = String.format("%1.2f", roundVol);
            } else if (Vol > 1000000000) {
                //System.out.println(product.getMarketCap());
                double Volumn = Vol / 1000000000.0;
                //System.out.println(String.valueOf(Cap));
                int tempVol = (int) Math.round(Volumn * 100);
                //System.out.println(String.valueOf(tempCap));
                double roundVol = tempVol / 100.0;
                //System.out.println(String.valueOf(roundCap));
                stock[7] = String.format("%1.2f", roundVol) + " Billion";
            } else {
                double Volumn = Vol / 1000000.0;
                int tempVol = (int) Math.round(Volumn * 100);
                double roundVol = tempVol / 100.0;
                stock[7] = String.format("%1.2f", roundVol) + " Million";
            }
            //
            double ChangeYTD = Double.valueOf(JsonSymbol.getString("ChangeYTD"));
            int tempChangeYTD = (int) Math.round(ChangeYTD * 100);
            double roundChangeYTD = tempChangeYTD / 100.0;
            double ChangePercentYTD = Double.valueOf(JsonSymbol.getString("ChangePercentYTD"));
            int tempChangePercentYTD = (int) Math.round(ChangePercentYTD * 100);
            double roundtempChangePercentYTD = tempChangePercentYTD / 100.0;
            stock[8] =  String.format("%1.2f", roundChangeYTD) + "(" + String.format("%1.2f", roundtempChangePercentYTD) + "%)";
            stock[9] = JsonSymbol.getString("High");
            stock[10] = JsonSymbol.getString("Low");
            stock[11] = JsonSymbol.getString("Open");
            stock[12] = JsonSymbol.getString("chart_link");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //System.out.println(stockList.get(0).length);
        CustomListAdapter adapter = new CustomListAdapter(getActivity(), Topic, stock);
        //list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                //to do something like get quote to go result activity
               if (arg2==12){
                   Dialog dialog = new Dialog(getActivity());
                   dialog.setContentView(R.layout.custom_dialog);

                   TouchImageView image = (TouchImageView) dialog.findViewById(R.id.image);
                   new showImage(image).execute(stock[12]);
                   dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
                   dialog.getWindow().getAttributes().height=600;
                   dialog.show();
               }
                /*if(arg2==12){
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    WebView webView = new WebView(getActivity());
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.setWebChromeClient(new WebChromeClient());
                    webView.addJavascriptInterface(new WebviewInterface(), "InterfaceImage");
                    webView.loadUrl("file:///android_asset/Dialog.html");
                    webView.setWebViewClient(new WebViewClient() {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            view.loadUrl(url);

                            return true;
                        }
                    });
                    alert.setView(webView);
                    alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }*/
            }
        });
    }
    public class WebviewInterface{
        @JavascriptInterface
        public String getYahooChart() throws JSONException {
            String imageChart = JsonSymbol.getString("chart_link");
            return imageChart;
        }
    }
}
class showImage extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;

    public showImage(ImageView bmImage) {
        this.bmImage = bmImage;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
    }
}

