package com.example.hw9;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class TopFreeFragment extends Fragment {
    private WebView webView;
    private JSONObject JsonSymbol;
    private JSONObject JsonChart;
    private String JsonNews;
    private TextView text;
    public TopFreeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_top_free, container, false);
        text = (TextView) view.findViewById(R.id.text);
        webView = (WebView) view.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.addJavascriptInterface(new WebviewInterface(), "InterfaceActivity");
        webView.loadUrl("file:///android_asset/MyHTML.html");
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ResultActivity resultActivity = (ResultActivity) getActivity();
        JsonSymbol = resultActivity.getJsonSymbol();
        JsonChart = resultActivity.getJsonChart();
        JsonNews = resultActivity.getJsonNews();
        try {
            text.setText(JsonSymbol.getString("Symbol"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public class WebviewInterface{
        @JavascriptInterface
        public String getJsonChart() throws JSONException {
            String Chart = JsonChart.toString();
            String str =
            Chart = Chart.substring(1);
            String ROM = "Symbol";
            ROM = "\"" + ROM + "\"";
            String COM = "\"" + JsonSymbol.getString("Symbol") + "\"";
            String NewChart = "{" + ROM + ":" + COM + "," + Chart;
            //System.out.println(NewChart);
            //System.out.println(JsonChart.toString());
            return NewChart;
        }
    }

}
