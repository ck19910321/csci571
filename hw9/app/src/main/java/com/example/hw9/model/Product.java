package com.example.hw9.model;

/**
 * Created by 邑安 on 2016/4/19.
 */
public class Product {
    /**
     * Status : SUCCESS
     * Name : Apple Inc
     * Symbol : AAPL
     * LastPrice : 106.95
     * Change : -0.530000000000001
     * ChangePercent : -0.49311499813919
     * Timestamp : Tue Apr 19 15:59:00 UTC-04:00 2016
     * MSDate : 42479.6659722222
     * MarketCap : 592993151850
     * Volume : 2269806
     * ChangeYTD : 105.26
     * ChangePercentYTD : 1.60554816644499
     * High : 107.91
     * Low : 106.25
     * Open : 107.91
     * chart_link : http://chart.finance.yahoo.com/t?s=aapl&lang=en-US&width=400&height=300
     */

    private String Status;
    private String Name;
    private String Symbol;
    private double LastPrice;
    private double Change;
    private double ChangePercent;
    private String Timestamp;
    private double MSDate;
    private long MarketCap;
    private int Volume;
    private double ChangeYTD;
    private double ChangePercentYTD;
    private double High;
    private double Low;
    private double Open;
    private String chart_link;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String Symbol) {
        this.Symbol = Symbol;
    }

    public double getLastPrice() {
        return LastPrice;
    }

    public void setLastPrice(double LastPrice) {
        this.LastPrice = LastPrice;
    }

    public double getChange() {
        return Change;
    }

    public void setChange(double Change) {
        this.Change = Change;
    }

    public double getChangePercent() {
        return ChangePercent;
    }

    public void setChangePercent(double ChangePercent) {
        this.ChangePercent = ChangePercent;
    }

    public String getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(String Timestamp) {
        this.Timestamp = Timestamp;
    }

    public double getMSDate() {
        return MSDate;
    }

    public void setMSDate(double MSDate) {
        this.MSDate = MSDate;
    }

    public long getMarketCap() {
        return MarketCap;
    }

    public void setMarketCap(long MarketCap) {
        this.MarketCap = MarketCap;
    }

    public int getVolume() {
        return Volume;
    }

    public void setVolume(int Volume) {
        this.Volume = Volume;
    }

    public double getChangeYTD() {
        return ChangeYTD;
    }

    public void setChangeYTD(double ChangeYTD) {
        this.ChangeYTD = ChangeYTD;
    }

    public double getChangePercentYTD() {
        return ChangePercentYTD;
    }

    public void setChangePercentYTD(double ChangePercentYTD) {
        this.ChangePercentYTD = ChangePercentYTD;
    }

    public double getHigh() {
        return High;
    }

    public void setHigh(double High) {
        this.High = High;
    }

    public double getLow() {
        return Low;
    }

    public void setLow(double Low) {
        this.Low = Low;
    }

    public double getOpen() {
        return Open;
    }

    public void setOpen(double Open) {
        this.Open = Open;
    }

    public String getChart_link() {
        return chart_link;
    }

    public void setChart_link(String chart_link) {
        this.chart_link = chart_link;
    }
}
