package com.example.hw9;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.hw9.model.AutoData;
import com.example.hw9.model.Product;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Set;
import android.os.Handler;

public class MainActivity extends AppCompatActivity {

    private static String url = "https://my-project-hw9-2.appspot.com/?";
    // for test
    private static String SYMBOL = "Symbol=";
    private static String CHART = "Chart=";
    private static String NEWS = "News=";
    private static String keyword = "keyword=";
    private Button btn1;
    private Button btn2;
    private ProgressBar loadingBar;
    private ToggleButton toggleButton;
    private ImageButton imageBtn;
    private AutoCompleteTextView editText;
    private TextView textView;
    private String key;
    private SwipeMenuListView list;
    private List<Product> products;
    private String [] item1;
    private String [] item2;
    private String [] item3;
    private String [] item4;
    private String [] item5;
    SharedPreferences sharedPreferences;
    FavorListAdapter customListAdapter;
    Runnable myRunnable;
    Handler handler;
    AutoListAdapter autoListAdapter;
    ProgressDialog progDailog;
    List<Product> productlist;
    private int mProgressStatus = 0;
    private Handler mHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadingBar = (ProgressBar) findViewById(R.id.loadingBar);
        loadingBar.setVisibility(View.GONE);
        editText = (AutoCompleteTextView) findViewById(R.id.edit_query);
        btn1 = (Button) findViewById(R.id.btn_quote);
        btn2 = (Button) findViewById(R.id.btn_clear);
        imageBtn = (ImageButton) findViewById(R.id.ImageBtn);
        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);
        list = (SwipeMenuListView) findViewById(android.R.id.list);
        doFavorList();
        autoListAdapter = new AutoListAdapter(MainActivity.this, new String[1] , new String[1]);
        editText.setAdapter(autoListAdapter);
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start>=2){
                    String keyup = s.toString();
                    // for test
                    String url1 = url + keyword + keyup;
                    new autoComplete().execute(url1);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                if (isChecked){
                     handler=  new Handler();
                     myRunnable = new Runnable() {
                        public void run() {
                            // do something
                            sharedPreferences = getSharedPreferences("Favorite", 0);
                            if (!sharedPreferences.getAll().isEmpty()) {
                                Set keys = sharedPreferences.getAll().keySet();
                                for (Object key : keys) {
                                    int i = 0;
                                    // do whatever
                                    if (sharedPreferences.contains((String) key)) {
                                        String symbolkey = (String) key;
                                        System.out.println(symbolkey  + "for doing refhres");
                                         //for test
                                        String refreshUrl =url + SYMBOL + symbolkey;
                                        new Refresh().execute(refreshUrl);
                                    }
                                }
                            }

                            handler.postDelayed(this, 10000);
                        }
                    };
                    handler.postDelayed(myRunnable,10);
                }
                else handler.removeCallbacks(myRunnable);
            }
        });
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                //deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(700);
                // set a icon
                //deleteItem.setIcon(R.drawable.ic_delete_grey600_48dp);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        list.setMenuCreator(creator);
        list.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
            }

            @Override
            public void onSwipeEnd(final int position) {
                if(position>=0){
                    AlertDialog.Builder altdial = new AlertDialog.Builder(MainActivity.this);
                    altdial.setMessage("Want to delete " + item1[position] + " from favorites?").setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    delete(item1[position]);
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    AlertDialog alert = altdial.create();
                    alert.show();
                }


            }
        });


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                //to do something like get quote to go result activity
                Log.d("############", "Items " + item1[arg2]);
                key = editText.getText().toString();
                String url1=url + SYMBOL + item1[arg2];
                String url2 = url + CHART + item1[arg2];
                String url3 = url + NEWS + item1[arg2];
                new getJson().execute(url1, url2, url3);
            }
        });
    }

    @Override
    public void onRestart() {
        super.onRestart();

        customListAdapter.notifyDataSetChanged();

    }

    public void onRefresh(View view){
        sharedPreferences = getSharedPreferences("Favorite", 0);
        if (!sharedPreferences.getAll().isEmpty()) {
            Set keys = sharedPreferences.getAll().keySet();
            for (Object key : keys) {
                int i = 0;
                // do whatever
                if (sharedPreferences.contains((String) key)) {
                    String symbolkey = (String) key;
                    System.out.println(symbolkey  + "for doing refhres");
                      //  for test
                    String refreshUrl =url + SYMBOL + symbolkey;
                    new Refresh().execute(refreshUrl);
                }
            }
        }
    }

    public void onClick(View view) {

        key = editText.getText().toString();
        if (key=="" || key==null || key.length()==0){
            System.out.println("key = " + key);
            System.out.println("key didn't find");
            AlertDialog.Builder altdial = new AlertDialog.Builder(MainActivity.this);
            altdial.setMessage("Please enter a Stock Name/Symbol").setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            AlertDialog alert = altdial.create();
            alert.show();
        }
        else {
            String url1=url + SYMBOL + key;
            String url2 = url + CHART + key;
            String url3 = url + NEWS + key;
            new getJson().execute(url1, url2, url3);
            sharedPreferences = getSharedPreferences("Favorite", 0);
        }
    }
    public void onClear(View view) {
        editText.setText("");
        /*sharedPreferences = getSharedPreferences("Favorite", 0);
        if (!sharedPreferences.getAll().isEmpty()) {
            Set keys = sharedPreferences.getAll().keySet();
            for (Object key : keys) {
                // do whatever
                if (sharedPreferences.contains((String) key)) {
                    String a = (String) key;
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.remove(a);
                    editor.commit();
                }
            }
        }
        doFavorList();*/
    }

    public void doFavorList() {
        sharedPreferences = getSharedPreferences("Favorite", 0);
        // if (!sharedPreferences.getAll().isEmpty()) {
        Gson gson = new Gson();
        Set keys = sharedPreferences.getAll().keySet();
        int index = keys.size();
        item1 = new String[index];
        item2 = new String[index];
        item3 = new String[index];
        item4 = new String[index];
        item5 = new String[index];
        int i = 0;
        for (Object key : keys) {
            // do whatever

            String symbolstr = sharedPreferences.getString((String) key, "");
            System.out.println(symbolstr);
            Product product = gson.fromJson(symbolstr, Product.class);
            System.out.println(product.getStatus());
            item1[i] = product.getSymbol();
            item2[i] = product.getName();
            // stock price round to 2 decimals
            double roundPrice = Math.round(product.getLastPrice() * 100.0) / 100.0;
            item3[i] = String.format("%1.2f", roundPrice);
            //Change percent rounded to 2 decimals
            double roundChange = Math.round(product.getChangePercent() *100.0) /100.0;
            item4[i] = String.format("%1.2f", roundChange);
            //Market Cap rounded to 2 decimals
            if (product.getMarketCap()<1000000){
                int tempCap = (int) Math.round(product.getMarketCap()*100);
                double roundCap = tempCap / 100.0;
                item5[i] = String.format("%1.2f", roundCap) + " ";
            }
            else if (product.getMarketCap()>1000000000){
                //System.out.println(product.getMarketCap());
                double Cap = product.getMarketCap()/1000000000.0;
                //System.out.println(String.valueOf(Cap));
                int tempCap = (int) Math.round(Cap*100);
                //System.out.println(String.valueOf(tempCap));
                double roundCap = tempCap / 100.0;
                //System.out.println(String.valueOf(roundCap));
                item5[i] =  String.format("%1.2f", roundCap)  + " Billion";
            }
            else {
                double Cap = product.getMarketCap()/1000000.0;
                int tempCap = (int) Math.round(Cap*100);
                double roundCap = tempCap / 100.0;
                item5[i] = String.format("%1.2f", roundCap)  + " Million";
            }

            i++;
            //System.out.println(sharedPreferences.getString((String) key, ""));
                /*if (sharedPreferences.contains((String) key)){
                    String a = (String) key;
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.remove(a);
                    editor.commit();
                }*/
        }

        customListAdapter = new FavorListAdapter(this, item1, item2, item3, item4, item5);

        //customListAdapter.notifyDataSetChanged();
        list.setAdapter(customListAdapter);
        customListAdapter.notifyDataSetChanged();
        //}
    }
    public void delete(String key){
        sharedPreferences = getSharedPreferences("Favorite", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
        System.out.println(key);
        doFavorList();
    }
    private void showProgressBar()
    {
        loadingBar.setVisibility(View.VISIBLE);
    }

    class Refresh extends AsyncTask<String, Void, JSONObject[]> {
        @Override
        protected void onPreExecute() {
            // show the progress bar
            showProgressBar();
            /*progDailog = new ProgressDialog(MainActivity.this);
            //progDailog.setMessage("Loading...");
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(true);
            progDailog.show();
            if (progDailog.isShowing()) {
                progDailog.dismiss();
            }*/
        }
        @Override
        protected JSONObject[] doInBackground(String... urls) {
            JSONParser jParser = new JSONParser();
            // Getting JSON from URL
            JSONObject[] jsons = new JSONObject[urls.length];
            for (int i = 0; i < urls.length; i++) {
                jsons[i] = jParser.JSONParse(urls[i]);
                // System.out.println(jsons[i].toString());
            }
            return jsons;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(JSONObject[] result) {
            sharedPreferences = getSharedPreferences("Favorite", 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            try {
                editor.putString(result[0].getString("Symbol"), result[0].toString());
                System.out.println("Put the new key" + result[0].getString("Symbol"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            editor.commit();
            doFavorList();
           loadingBar.setVisibility(View.GONE);
        }
    }

    class getJson extends AsyncTask<String, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgressBar();
        }

        @Override
        protected String[] doInBackground(String... urls) {
            JSONParser jParser = new JSONParser();

            // Getting JSON from URL
            String[] jsons = new String[urls.length];
            for (int i = 0; i < urls.length; i++) {
                jsons[i] = jParser.JSONParseToString(urls[i]);
                // System.out.println(jsons[i].toString());
            }

            return jsons;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String[] result) {
            //JSONParser jParser = new JSONParser();
            //JSONObject jsons = jParser.JSONParse(result[0]);
            //System.out.println(jsons.toString());
            //System.out.println(result[1].toString() + "ken" + result[1].length());

            if (result[0].length()<300){
                AlertDialog.Builder altdial = new AlertDialog.Builder(MainActivity.this);
                altdial.setMessage("Invalid Symbol").setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = altdial.create();
                alert.show();
                return;
            }
            else {
                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                intent.putExtra("symbol", result[0]);
                intent.putExtra("chart", result[1]);
                intent.putExtra("news", result[2]);
                startActivity(intent);
            }
            //loadingBar.setVisibility(View.GONE);
        }
    }
    class autoComplete extends AsyncTask<String, Void, JSONArray> {

        @Override
        protected JSONArray doInBackground(String... urls) {
            JsonParserArray jParser = new JsonParserArray();
            String url = urls[0];
            // Getting JSON from URL
            JSONArray jsonarray =jParser.JSONParseArray(url);
            return jsonarray;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(JSONArray jsonArray) {
            Gson gson = new Gson();
            if (jsonArray!=null){
                int size = jsonArray.length();
                String [] label = new String[size];
                String [] value = new String[size];
                for (int i = 0; i < size; i++) {
                    //JSONArray innerJsonArray = null;
                    try {
                        //JSONObject jsonObject = jsonArray.getJSONArray(i);
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        AutoData autoData = gson.fromJson(jsonObject.toString(), AutoData.class);
                        value[i]=autoData.getValue();
                        label[i]=autoData.getLabel();
                        System.out.println(value[i] + " " + label[i]);
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }

                }
                autoListAdapter = new AutoListAdapter(MainActivity.this, value, label);
                editText.setAdapter(autoListAdapter);
                autoListAdapter.notifyDataSetChanged();
            }

        }
    }
}


