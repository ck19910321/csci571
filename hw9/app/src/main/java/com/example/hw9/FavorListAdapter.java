package com.example.hw9;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FavorListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] item1;
    private final String[] item2;
    private final String[] item3;
    private final String[] item4;
    private final String[] item5;
    LayoutInflater inflater;

    //private final Integer[] imgid;

    public FavorListAdapter(Activity context, String[] item1, String[] item2, String[] item3, String[] item4, String[] item5) {
        super(context, R.layout.my_fav_list, item1);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.item1=item1;
        this.item2=item2;
        this.item3=item3;
        this.item4=item4;
        this.item5=item5;
        //this.imgid=imgid;
    }
    /*public class ViewHolder{
        TextView txtSymbol;
        TextView txtName;
        TextView txtPrice;
        TextView txtChange;
        TextView txtMarCap;
    }*/

    public View getView(int position,View convertView,ViewGroup parent) {


        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.my_fav_list, null,true);
        //ViewHolder holder = new ViewHolder();
        TextView txtSymbol = (TextView) rowView.findViewById(R.id.textSymbol);
        TextView txtName = (TextView) rowView.findViewById(R.id.textName);
        TextView txtPrice = (TextView) rowView.findViewById(R.id.textView);
        TextView txtChange = (TextView) rowView.findViewById(R.id.textView2);
        TextView txtMarCap = (TextView) rowView.findViewById(R.id.textView3);

        txtSymbol.setText(item1[position]);
        //imageView.setImageResource(imgid[position]);
        txtName.setText(item2[position]);
        txtPrice.setText("$ " + item3[position] +"      ");

        txtChange.setText("    " + item4[position] + "%   " );
        if (Double.valueOf(item4[position])>0){
            txtChange.setBackgroundColor(Color.GREEN);
        }
        else if (Double.valueOf(item4[position])<0)
            txtChange.setBackgroundColor(Color.RED);
        txtMarCap.setText("Market Cap : " + item5[position]);

        return rowView;

    }


}

