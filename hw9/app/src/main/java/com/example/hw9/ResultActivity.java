package com.example.hw9;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hw9.model.Product;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import com.facebook.FacebookSdk;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;


public class ResultActivity extends AppCompatActivity {
    JSONObject jsonSymbol,jsonChart =null;
    String jsonNews;
    SharedPreferences sharedPreference;
    Toolbar toolbar;
    TabLayout tabLayout;
    Button btn;
    ImageView imageView;
    boolean isPressed;
    SwipeDisableViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    SharedPreferences sharedPreferences;
    private ImageButton imageBtn;
    Product product;
    Gson gson;
    private CallbackManager callbackManager;
    private LoginManager manager;
    private ShareLinkContent shareLinkContent;
    private ShareDialog shareDialog;
    private String facebook_symbol_name;
    private String facebook_symbol_price;
    private String facebook_symbol_chart_link;
    @Override
    protected void onCreate (Bundle savedInstanceState){
        FacebookSdk.sdkInitialize(getApplicationContext());

        /*try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.example.hw9",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }*/
        gson = new Gson();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_layout);
        toolbar = (Toolbar) findViewById(R.id.toolBar);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (SwipeDisableViewPager) findViewById(R.id.viewPager);
        viewPager.setSwipeable(false);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new HomeFragment(), "Current");
        viewPagerAdapter.addFragments(new TopFreeFragment(),"Historical");
        viewPagerAdapter.addFragments(new TopPaidFragment(), "News");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
         try {
             Intent intent = getIntent();
            jsonSymbol = new JSONObject(intent.getStringExtra("symbol"));
             facebook_symbol_name = jsonSymbol.getString("Name");
             facebook_symbol_price = jsonSymbol.getString("LastPrice");
             facebook_symbol_chart_link = jsonSymbol.getString("chart_link");
            //System.out.println("Symbol" + jsonSymbol.getString("Symbol"));
            jsonChart = new JSONObject(intent.getStringExtra("chart"));
            jsonNews = intent.getStringExtra("news");
             product = gson.fromJson(jsonSymbol.toString(), Product.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setTitle(product.getName());
        sharedPreferences = getSharedPreferences("Favorite", 0);
        imageBtn = (ImageButton) findViewById(R.id.imageBtn);

    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.my_menu, menu);
        if(checkFavor()){
            menu.findItem(R.id.Favor_icon).setIcon(R.drawable.starfull);
        }

        return true;
    }
    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (resultCode!=0){
            Toast.makeText(getApplicationContext(),
                    "You shared this post",
                    Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){

            case R.id.Favor_icon:
                String tag = imageBtn.getTag().toString();
                if(onClickFavor()){
                    item.setIcon(R.drawable.starfull);
                }
                else item.setIcon(R.drawable.ic_star_border_black_24dp);
                return true;
            case R.id.facebook:
                try {
                    Toast.makeText(getApplicationContext(),
                            "Sharing " + jsonSymbol.getString("Name") + " !!!",
                            Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                callbackManager = CallbackManager.Factory.create();
                shareDialog = new ShareDialog(this);
                // this part is optional
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                    }
                    @Override
                    public void onCancel() {
                    }
                    @Override
                    public void onError(FacebookException error) {
                    }
                });
                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentTitle("Current Stock Price of " + facebook_symbol_name + ", " + facebook_symbol_price)
                            .setContentDescription(
                                    "Stock Information of" + facebook_symbol_name)
                            //.setContentUrl("Stock Market Viewer")
                            .setImageUrl(Uri.parse(facebook_symbol_chart_link))
                            .build();

                    shareDialog.show(linkContent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean checkFavor(){
        imageBtn.setTag("empty");
        if (!sharedPreferences.getAll().isEmpty()) {
            Set keys = sharedPreferences.getAll().keySet();
            if (keys.contains(product.getSymbol())) {
                imageBtn.setTag("full");
                imageBtn.setImageResource(R.drawable.ic_star_black_24dp);
                return true;
            }
        }
        return false;
    }
    public boolean onClickFavor() {

        sharedPreferences = getSharedPreferences("Favorite", 0);
        Set keys = sharedPreferences.getAll().keySet();
        String tag = imageBtn.getTag().toString();
        if (tag.equalsIgnoreCase("empty")){
            try {
                Toast.makeText(getApplicationContext(),
                        "Bookedmarked  " + jsonSymbol.getString("Name") + " !!",
                        Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            imageBtn.setTag("full");
            //System.out.println(symbolstr);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(product.getSymbol(), jsonSymbol.toString());
            editor.commit();
            return true;
        }

        else {

            imageBtn.setTag("empty");
            for (Object key : keys) {
                // do whatever
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove(product.getSymbol());
                editor.commit();
            }
            return false;
        }

    }
    public JSONObject getJsonSymbol() {
        return jsonSymbol;
    }
    public JSONObject getJsonChart() {
        return jsonChart;
    }
    public String getJsonNews() {
        return jsonNews;
    }


}
