package com.example.hw9;

/**
 * Created by 邑安 on 2016/4/19.
 */
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CustomListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] item1;
    private final String[] item2;
    //private final Integer[] imgid;

    public CustomListAdapter(Activity context, String[] item1, String[] item2) {
        super(context, R.layout.mylist, item1);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.item1=item1;
        this.item2=item2;
        //this.imgid=imgid;
    }

    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.mylist, null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.item);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        TextView extratxt = (TextView) rowView.findViewById(R.id.textView1);
        ImageView imageArrow = (ImageView)rowView.findViewById(R.id.arrow);
        if (position==4 || position==8){
            String str =item2[position];
            Double percent = Double.valueOf(str.substring(str.indexOf('(')+1,str.indexOf(')')-1));
            System.out.println(str.substring(str.indexOf('(')+1,str.indexOf(')')-1));
                    //If we find a non-digit character we return false.
                    if (percent<0.0){
                        imageArrow.setImageResource(R.drawable.down);
                    }
                    else  if (percent>0.0){
                        imageArrow.setImageResource(R.drawable.up);
                    }

        }

        if (position==12){
            txtTitle.setText(item1[position]);
            new DownloadImageTask(imageView).execute(item2[position]);
            extratxt.setText("");
        }
        else {
            txtTitle.setText(item1[position]);
            //imageView.setImageResource(imgid[position]);
            extratxt.setText(item2[position]);
        }

        return rowView;

    };
}
class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;

    public DownloadImageTask(ImageView bmImage) {
        this.bmImage = bmImage;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
    }
}
