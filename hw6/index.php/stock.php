<!DOCTYPE html>
<html>
<head>
<title>PHP Parser</title>
 <script>
        
        function clearForm(){
            document.getElementById("output").innerHTML = "";
            document.getElementById("text").value="";
        }
        /*function checkbox(){
            var text = document.getElementById("text").value;
            if (text==null||text==""){
                alert("Please enter Name or Symbol");
                return false;
            }
            else return true;
        }*/
</script>
<style type="text/css">
    .firstTable {
        font-family: Times New Roman;
        border: 1.5px solid lightgray;
        border-collapse: collapse;
        background-color: #f2f2f2;
        width: 450px;
    }
    hr {
        height:1px;
        margin-top: -0.2em;
        color: lightgray;
        background-color: lightgray;
        border: none;
}
    .firstRow{
        vertical-align: bottom;
        text-align:center;
        font-size: 30px;
        font-style: italic;
    }
    .text{
        font-size:18px;
        text-align:left;
        font-family:Times New Roman;
        background-color: #f2f2f2;
    }
    .link{
        font-size:18px;
        text-align:center;
    }
    .secondTable{
            border:1px;
            font-family: sans-serif;
            background-color:#FAFAFA;
            border-collapse: collapse;
            padding:5px;
            width:600px;
    }
    .secondTable td {
        border: 1px solid lightgray;
        font-size:13px;
    }
        
    .thirdTable {
        border: 2px solid lightgray;
            border-collapse: collapse;
            padding:5px;
    }
    th.s1 {
        text-align:left;
        font-size: 13px;
        border: 2px solid lightgray;
        background-color:#F3F3F3;
    }
    th.s3 {
        font-size:13px;
        height:20px;
        border: 1px solid lightgray;
        text-align: left;
        font-family: sans-serif;
        width: 300px;
        background-color:#F3F3F3;
    }
    td.g3 {
        height:20px;
        font-size:13px;
        border: 1px solid lightgray;
        font-family: sans-serif;
        text-align: center;
        width: 300px;
        background-color: #FAFAFA;
    }
    table.results td{
        border:2.5px solid lightgray;
        background-color:#FAFAFA;
        width:600px;
        text-align:center;
    }
    </style>
</head>    
<body>
    <center>
    <form id="search" method="GET" onsubmit="return checkbox();">
    <table class="firstTable">
        <tr>
            <td class="firstRow" colspan=2>Stock Search</td>
        </tr>
        <tr><td colspan=2 style="height="><hr></td></tr>
        <tr><td class="text">Company Name or Symbol:<input type="text" id="text" name="text" value="<?php echo isset($_GET["text"]) ? $_GET["text"] : "" ?>" oninvalid="alert('Please enter Name or Symbol');" required></td>
        <tr><td><INPUT  type="submit" VALUE="Search" name = "submit" style="position:relative; right:-202px; background-color:#FFFFFF; border-radius: 4px;box-shadow:none;"></td>
            <td><input type="button" value="Clear" name = "clear"style="position:relative; right:120px; background-color:#FFFFFF; border-radius: 4px;box-shadow:none;" onclick="clearForm()"></td></tr>
        <tr><td class="link" colspan=2><a href="http://www.markit.com/product/markit-on-demand" target="_blank">Powered by Markit on Demend</a></td></tr>
        <tr><td><br></td></tr>
        </table>
        </form>
        <div id="output">
   <?php if(isset($_GET["submit"])): ?>
    <?php 
        $queryResults = "";
        $text = $_GET["text"];
        $urlA = rawurlencode("http://dev.markitondemand.com/MODApis/Api/v2/Lookup/xml?");
        $urlB = urlencode("input=".$_GET["text"]);
        $tot_url = $urlA . $urlB; 
        $xml = simplexml_load_file($tot_url); 
        if (!$xml){
                $queryResults = "No Records has been found";
                echo "<br>";
                echo "<table class=\"results\">";
                echo "<tr><td>$queryResults</td></tr></table>";            
            }
        else{
        echo "<br>";
        echo "<table border=1px  class=\"secondTable\">";        
        echo "<tr><th class=\"s1\">Name</th>";
        echo "<th class=\"s1\">Symbol</th>";
        echo "<th class=\"s1\">Exchange</th>";
        echo "<th class=\"s1\">Details</th></tr>";
            foreach($xml->children() as $list) { 
                echo "<tr><td>" . $list->Name . "</td>"; 
                echo "<td>" . $list->Symbol . "</td>"; 
                echo "<td>" . $list->Exchange . "</td>";
                $sym = (string) $list->Symbol;
                $tempurl = "?Symbol=$sym";
                echo "<td><a href = ?Symbol=$sym>More Info</a></td></tr>";
            } 
        }?>
        
        <?php endif;?>
        <?php if(isset($_GET['Symbol'])) {
            $symurl = "http://dev.markitondemand.com/MODApis/Api/v2/Quote/json?symbol=$_GET[Symbol]";
            $jsondata = file_get_contents($symurl);
            $obj = json_decode($jsondata);
            if ($obj->{"Status"}!="SUCCESS"){
                $queryResults = "There is no stock information available";
                echo "<br>";
                echo "<table class=\"results\">";
                echo "<tr><td>$queryResults</td></tr></table>";  
            }
            else {
            echo "<br>";
            echo "<table border=1 class=\"thirdTable\">";
            $name = $obj->{"Name"};
            echo "<tr><th class=\"s3\">Name</th> <td class=\"g3\">$name</td></tr>";
            $symbolName = $obj->{"Symbol"};
            echo "<tr><th class=\"s3\">Symbol</th> <td class=\"g3\">$symbolName</td></tr>";
            $LPrice = $obj->{"LastPrice"};
            echo "<tr><th class=\"s3\">Last Price:</th> <td class=\"g3\">$LPrice</td></tr>";
            $Change = $obj->{"Change"};
            if (round($Change, 2)>0){
                echo "<tr><th class=\"s3\">Change</th> <td class=\"g3\">" .round($Change, 2)."<img src=\"http://cs-server.usc.edu:45678/hw/hw6/images/Green_Arrow_Up.png\" width=\"15\" height=\"15\" /></td></tr>";
            }
            else if (round($Change, 2)<0) {
                echo "<tr><th class=\"s3\">Change</th> <td class=\"g3\">" .round($Change, 2)."<img src=\"http://cs-server.usc.edu:45678/hw/hw6/images/Red_Arrow_Down.png\" width=\"15\" height=\"15\" /></td></tr>";
            }
            else {
                echo "<tr><th class=\"s3\">Change</th> <td class=\"g3\">" .round($Change, 2)."</td></tr>";
            }

    
            $ChangePercent = $obj->{"ChangePercent"};
            if (round($ChangePercent, 2)>0){
                echo "<tr><th class=\"s3\">Change Percent</th> <td class=\"g3\">" .round($ChangePercent, 2)."%<img src=\"http://cs-server.usc.edu:45678/hw/hw6/images/Green_Arrow_Up.png\" width=\"15\" height=\"15\" /></td></tr>";
            }
            else if (round($ChangePercent, 2)<0) {
                echo "<tr><th class=\"s3\">Change Percent</th> <td class=\"g3\">" .round($ChangePercent, 2)."%<img src=\"http://cs-server.usc.edu:45678/hw/hw6/images/Red_Arrow_Down.png\" width=\"15\" height=\"15\" /></td></tr>";
            }
            else {
                echo "<tr><th class=\"s3\">Change Percent</th> <td class=\"g3\">" .round($ChangePercent, 2)."%</td></tr>";
            }
            
            $Timestamp = $obj->{"Timestamp"};
            $d = new DateTime($Timestamp);
            
            echo "<tr><th class=\"s3\">Timestamp</th> <td class=\"g3\">".$d->format('Y-m-d\ H:i:s A')."</td></tr>";
            $temp = $obj->{"MarketCap"};
            if ($temp<1000000){
                $MarketCap = $temp/1000000;
                echo "<tr><th class=\"s3\">Market Cap</th> <td class=\"g3\">" .round($MarketCap, 2)."M</td></tr>";
            }
            else {
                $MarketCap = $temp/1000000000;
                echo "<tr><th class=\"s3\">Market Cap</th> <td class=\"g3\">" .round($MarketCap, 2)."B</td></tr>";
            }
            $Volume = $obj->{"Volume"};
            $english_format_number = number_format($Volume);
            echo "<tr><th class=\"s3\">Volume</th> <td class=\"g3\">$english_format_number</td></tr>";
            $ChangeYTD = $obj->{"ChangeYTD"};
            $ans_ChangeYTD = $LPrice - $ChangeYTD;
            
            if (round($ans_ChangeYTD, 2)>0){
                echo "<tr><th class=\"s3\">Change YTD</th> <td class=\"g3\">" .round($ans_ChangeYTD, 2). "<img src=\"http://cs-server.usc.edu:45678/hw/hw6/images/Green_Arrow_Up.png\" width=\"15\" height=\"15\" /></td></tr>";
            }
            else if (round($ans_ChangeYTD, 2)<0) {
                echo "<tr><th class=\"s3\">Change YTD</th> <td class=\"g3\">(" .round($ans_ChangeYTD, 2). ")<img src=\"http://cs-server.usc.edu:45678/hw/hw6/images/Red_Arrow_Down.png\" width=\"15\" height=\"15\" /></td></tr>";
            }
            else {
                echo "<tr><th class=\"s3\">Change YTD</th> <td class=\"g3\">" .round($ans_ChangeYTD, 2). "</td></tr>";
            }
            $ChangePercentYTD = $obj->{"ChangePercentYTD"};
            if (round($ChangePercentYTD, 2)>0){
                echo "<tr><th class=\"s3\">Change Percent YTD</th> <td class=\"g3\">" .round($ChangePercentYTD, 2)."%<img src=\"http://cs-server.usc.edu:45678/hw/hw6/images/Green_Arrow_Up.png\" width=\"15\" height=\"15\" /></td></tr>";
            }
            else if (round($ChangePercentYTD, 2)<0) {
                echo "<tr><th class=\"s3\">Change Percent YTD</th> <td class=\"g3\">" .round($ChangePercentYTD, 2)."%<img src=\"http://cs-server.usc.edu:45678/hw/hw6/images/Red_Arrow_Down.png\" width=\"15\" height=\"15\" /></td></tr>";
            }
            else {
                echo "<tr><th class=\"s3\">Change Percent YTD</th> <td class=\"g3\">" .round($ChangePercentYTD, 2)."%</td></tr>";
            }
            $High = $obj->{"High"};
            echo "<tr><th class=\"s3\">High</th> <td class=\"g3\">$High</td></tr>";
            $Low = $obj->{"Low"};
            echo "<tr><th class=\"s3\">Low</th> <td class=\"g3\">$Low</td></tr>";          
            $Open = $obj->{"Open"};
            echo "<tr><th class=\"s3\">Open</th> <td class=\"g3\">$Open</td></tr>";
            echo "</table>";
            //var_dump($jsondata);
            }
            
}
            ?>
            
            
    
            </div>        
            
            
            
 </center><NOSCRIPT></body></html>