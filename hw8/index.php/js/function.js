$(document).ready(function(){
    updateFavor();
})

var MIN_LENGTH = 0;
var globalJsonObject;
  
<!-------------------autocomplete version 1.0------------------->

$(document).ready(function(){
    $("#text").autocomplete({
        source: (function( request, response ) {
            $.ajax({
                dataType: "json",
                type : 'GET',
                url: 'index.php',
                data:{keyword:request.term},
                success: (function(data) {
                  data = jQuery.parseJSON(data);
                  response($.map(data, function (item) {
                                return {
                                    label: item.label,
                                    value: item.value,
                                };
                    }));
                })

            })
        })
    })
})
<!----------------autocomplete version 1.0--------------->

    var errorAlert ="Select a valid entry";          
       
    function checkEmpty() {        
        
        var image = document.getElementById('favorite');
        var text = document.getElementsByName("text")[0].value;
        if (text.length!=0){
            var queryString = "?text=" + text;
    $.getJSON('index.php' + queryString, function (data) { 
			 var json_obj = $.parseJSON(data);//parse JSON 
                //console.log(json_obj);
                //var str =json_obj.Message.match(/No symbol/g);
                if (json_obj.Status==null){
                    $("#list").text(errorAlert).css("color", "red");
                    //document.getElementById("list").innerHTML="<span id='validate'>" + errorAlert + "</span>";
                    //console.log(errorAlert);
                    return false;
                }
                else{
            document.getElementById("list").innerHTML="";
            requestJson();
            requestChart();
            requestNews();
            //localStorage.clear();
            $("#myBtn2").prop('disabled', false);
            $("#myCarousel").carousel("next");
            return true;
                }
        })
        }
        }
  
       function clearForm(){

           //localStorage.clear();
            //document.getElementById("output").innerHTML = "";
            document.getElementById("text").value="";
           document.getElementById("list").innerHTML="";
           $("#myBtn2").prop('disabled', true);
           $("#myCarousel").carousel("prev");
        }
    
    <!-----------------request favorite----------->
    <!-----------------request favorite----------->

    //get json file from php
    function requestJsonByFavor(symbol){
        var image = document.getElementById('favorite');
        //alert(symbol);
        var queryString = "?Symbol=" + symbol;
    $.getJSON('index.php' + queryString, function (data){
        var json_obj = $.parseJSON(data);//parse JSON 
                if (localStorage.getItem(json_obj.Symbol)){
                        image.src = "fullStar.png";
                } else image.src = "emptyStar.png";
                globalJsonObject = json_obj;
                write(json_obj);
        })
			 
    }    
    function requestJson() {
        //localStorage.clear();
        
        var text = document.getElementsByName("text")[0].value;
        var image = document.getElementById('favorite');
        //var queryString = "?Symbol=" + text;
			var queryString = "?Symbol=" + text;
    $.getJSON('index.php' + queryString, function (data){
			 var json_obj = $.parseJSON(data);//parse JSON 
                if (localStorage.getItem(json_obj.Symbol)){
                        image.src = "fullStar.png";
                } else image.src = "emptyStar.png";
                globalJsonObject = json_obj;
                write(json_obj);
        })
    }

<!-------------Favorite icon---------------->

        
function changeImage() {
    var currentSymbol = globalJsonObject.Symbol;
    //alert(globalJsonObject.Symbol);
    
    if (localStorage.getItem(currentSymbol)) {
        var image = document.getElementById('favorite');
        image.src = "emptyStar.png";
        localStorage.removeItem(currentSymbol);
        updateFavor();
        //updateFavor();
    } else {
        var image = document.getElementById('favorite');
        image.src = "fullStar.png";
        localStorage.setItem(currentSymbol, "1");
        writeFavor(globalJsonObject);
    } 
}
    

<!-------------Favorite icon---------------->

   
   $(document).ready(function(){
    // Look for any event 'keydown' in any element which has a class 'formatCSS'
    $(document).on('click','.trash',function(e){
           var id=$(this).attr('id');
        //alert(id);
        var rowID = document.getElementById(id);
        //alert(rowID);
        rowID.parentNode.removeChild(rowID);
        localStorage.removeItem(id);
        if (globalJsonObject.Symbol==id){
            var image = document.getElementById('favorite');
            image.src = "emptyStar.png";
        }
    })
   })
    $(document).ready(function(){
    // Look for any event 'keydown' in any element which has a class 'formatCSS'
    $(document).on('click','.symbolName',function(e){
           var id=$(this).attr('id');
        //alert(id);
    requestJsonByFavor(id);
    requestChartByFavor(id);
    requestNewsByFavor(id);
    $("#myCarousel").carousel("next");
    })
   })

function writeFavor(obj){
           var html = "";
           var name = obj.Name;
           var sname = obj.Symbol;
           var lprice = obj.LastPrice;
html += "<tr class='myTableRow' id=" + obj.Symbol + "><td><a class='symbolName' href='#' id=" + obj.Symbol + ">" +sname + "</a></td>";
html += "<td>" + name + "</td>";
html += "<td>$ " + lprice.toFixed(2) + "</td>";
           var change = obj.Change;
           var changeR = change.toFixed(2); 
           var changeP = obj.ChangePercent;
           var changePR = changeP.toFixed(2);
           if (changePR<0){
               html += "<td><span class=decrease>" + changeR +" ( " + changePR + "% )</span><img src='http://cs-server.usc.edu:45678/hw/hw8/images/down.png' style='width:15;height:15'></td>";
           }
           else if (changePR>0) {
               html += "<td><span class=increase>" + changeR +" ( " + changePR + "% )</span><img src='http://cs-server.usc.edu:45678/hw/hw8/images/up.png' style='width:15;height:15'></td>";
           }
           else {
                html += "<td><span>" + changeR +" ( " + changePR + "% )</span></td>";
           }
            var marketCap = obj.MarketCap/1000000000;
           var marketCapR = marketCap.toFixed(2);
           html += "<td>" + marketCapR + " Billion</td>";
            html +="<td><button id=" + obj.Symbol + " type='button' class='trash' title='trash'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button></td></tr>";
          $('#favorTable').append(html);
       }    

<!---------------delete button-------------->

       function write(obj){
           var html = "";
           var name = obj.Name;
           var sname = obj.Symbol;
           var lprice = obj.LastPrice;
html += "<table class='table table-striped'><tr><th>&nbsp;&nbsp;Name</th><td id='favorName'>" +name + "</td></tr>";
html += "<tr><th>&nbsp;&nbsp;Symbol</th><td id='favorSName'>" + sname + "</td></tr>";
html += "<tr><th>&nbsp;&nbsp;Last Price</th><td id='favorLP'>$ " + lprice.toFixed(2) + "</td></tr>";
           var change = obj.Change;
           var changeR = change.toFixed(2); 
           var changeP = obj.ChangePercent;
           var changePR = changeP.toFixed(2);
           if (changePR<0){
               html += "<tr><th>&nbsp;&nbsp;Change(Change Percent)</th><td><span class=decrease>" + changeR +" ( " + changePR + "% )</span><img src='http://cs-server.usc.edu:45678/hw/hw8/images/down.png' style='width:15;height:15'></td></tr>";
           }
           else if (changePR>0) {
               html += "<tr><th>&nbsp;&nbsp;Change(Change Percent)</th><td><span class=increase>" + changeR +" ( " + changePR + "% )</span><img src='http://cs-server.usc.edu:45678/hw/hw8/images/up.png' style='width:15;height:15'></td></tr>";
           }
           else {
                html += "<tr><th>&nbsp;&nbsp;Change(Change Percent)</th><td><span>" + changeR +" ( " + changePR + "% )</span></td></tr>";
           }
           
           var date = new Date(obj.Timestamp);
           
           var monthNames = [
  "January", "February", "March",
  "April", "May", "June", "July",
  "August", "September", "October",
  "November", "December"
];
           var day = date.getDate();
           if (day<10){
               day="0" + day;
           }
           var monthIndex = date.getMonth();
           var year = date.getFullYear();
           var hour = date.getHours();
           if (hour>=12){
               var TT=" pm";
           }
           else var TT=" am";
           var min = date.getMinutes();
           var sec = date.getSeconds();
           if (hour<10){
                    hour = "0" + hour;
                }
                var min = date.getMinutes();
                if (min<10){
                    min = "0" + min;
                }
                var sec = date.getSeconds();
                if (sec <10){
                    sec = "0" + sec;
                }
var time = day + ' ' + monthNames[monthIndex] + ' ' + year + ', ' + hour + ':' + min + ':' + sec  + TT;

           html += "<tr><th>&nbsp;&nbsp;Time and Date</th><td>" + time + "</td></tr>";
           var marketCap = obj.MarketCap/1000000000;
           var marketCapR = marketCap.toFixed(2);
           html += "<tr><th>&nbsp;&nbsp;Market Cap</th><td id='favorMarket'>" + marketCapR + " Billion</td></tr>";
           var volume = obj.Volume;
           html += "<tr><th>&nbsp;&nbsp;Volume</th><td>" + volume + "</td></tr>";
           var ChangeYTD = obj.ChangeYTD.toFixed(2);
           var ChangePercentYTD = obj.ChangePercentYTD;
           if (ChangePercentYTD>0){
               html += "<tr><th>&nbsp;&nbsp;Change YTD(Change Percent YTD)</th><td><span class=increase>" + ChangeYTD +" ( " + ChangePercentYTD.toFixed(2) + "% )</span><img src='http://cs-server.usc.edu:45678/hw/hw8/images/up.png' style='width:15;height:15'></td></tr>";
           }
           else if (ChangePercentYTD<0) {
               html += "<tr><th>&nbsp;&nbsp;Change YTD(Change Percent YTD)</th><td><span class=decrease>" + ChangeYTD +" ( " + ChangePercentYTD.toFixed(2) + "% )</span><img src='http://cs-server.usc.edu:45678/hw/hw8/images/down.png' style='width:15;height:15'></td></tr>";
           }
           else {
               html += "<tr><th>&nbsp;&nbsp;Change(Change Percent)</th><td><span>" + ChangeYTD +" ( " + ChangePercentYTD.toFixed(2) + "% )</span></td></tr>";
           }
           var High = obj.High.toFixed(2);
           var Low = obj.Low.toFixed(2);
           var Open = obj.Open.toFixed(2);
           html += "<tr><th>&nbsp;&nbsp;High Price</th><td>$ " + High + "</td></tr>";
           html += "<tr><th>&nbsp;&nbsp;Low Price</th><td>$ " + Low + "</td></tr>";
           html += "<tr><th>&nbsp;&nbsp;Open Price</th><td>$ " + Open + "</td></tr></table>";
           
           document.getElementById("secondresult").innerHTML = html; 
           
           document.getElementById("chartDiv").innerHTML = 
               "<img src=\"" + obj.chart_link + "\" alt=\"Smiley face\" height=\"400\" width=\"600\">"; 
           
       }
$(document).ready(function(){
    // Activate Carousel
    $("#myCarousel").carousel("pause");

    // Go to the previous item
    $("#myBtn").click(function(){
        $("#myCarousel").carousel("prev");
    });

    // Go to the next item
    $("#myBtn2").click(function(){
        $("#myCarousel").carousel("next");
    });

});
<!---------------------refresh column--------------->
    $(document).on('click','#reclear',function(e){
    //alert("!!");
        updateFavorCol();
    })
<!---------------------refresh column--------------->

<!-----------------toggle button event---------------->
    var time = null;
$(function() {
    $('#toggle-event').change(function() {
        if($(this).prop('checked')){
                //alert("!!!!");
                time = setInterval(function() {
                    //updateColFavor();
                    updateFavorCol();
                    }, 5000);
                 
        }
        else {
            
            clearInterval(time);
        }
        })
})

function requestChartByFavor(symbol){
    var queryString = "?Chart=" + symbol;
    $.getJSON('index.php' + queryString, function (data) { 
        
        var obj = $.parseJSON(data);
        //var date = new Date(obj.Dates);
        // Create the chart
        var a = obj.Dates;
        var b = obj.Elements[0].DataSeries.close.values;
        var c = a.map(function (e, i) {
        return [a[i], b[i]];
        }); 
        //alert(c);
        for (var i = 0; i < c.length; i++ ){ 
            c[i][0] = new Date(c[i][0]).getTime();
        }
        //console.log(c);
        $('#HightChartStock').highcharts('StockChart', {
            chart: {
					renderTo: 'HightChartStock',										width: 1100															
				},
            rangeSelector : {
                allButtonsEnabled: true,
                buttons: [{
                    type:'week',
                    count:1,
                    text:'1w'
                },
                {
	           type: 'month',
	           count: 1,
	           text: '1m'
                }, {
	           type: 'month',
	           count: 3,
	           text: '3m'
                }, {
	           type: 'month',
	           count: 6,
	           text: '6m'
                }, {
	           type: 'ytd',
	           text: 'YTD'
                }, {
	           type: 'year',
	           count: 1,
	           text: '1y'
                }, {
	type: 'all',
	text: 'All'
}],
               inputEnabled: false,
                selected: 0
            },

            title : {
                text : symbol +" Stock Value"
            },
            xAxis: {
                    
                    categories: obj.Positions
                },
            series : [{
                name : symbol + ' Stock Price',
                data : c,
                type : 'area',
                threshold : null,
                tooltip : {
                    valueDecimals : 2
                },
                fillColor : {
                    linearGradient : {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops : [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                }
            }],
           exporting: {
        buttons: {
            contextButton: {
                enabled: false
            }    
        }
    }
        });
    });
}
function requestChart(){
   var text = document.getElementsByName("text")[0].value;
        var queryString = "?Chart=" + text;
    $.getJSON('index.php' + queryString, function (data) { 
        
        var obj = $.parseJSON(data);
        //var date = new Date(obj.Dates);
        // Create the chart
        var a = obj.Dates;
        var b = obj.Elements[0].DataSeries.close.values;
        var c = a.map(function (e, i) {
        return [a[i], b[i]];
        }); 
        //alert(c);
        for (var i = 0; i < c.length; i++ ){ 
            c[i][0] = new Date(c[i][0]).getTime();
        }
        //console.log(c);
        $('#HightChartStock').highcharts('StockChart', {
            chart: {
					renderTo: 'HightChartStock',										width: 1100															
				},
            rangeSelector : {
                allButtonsEnabled: true,
                buttons: [{
                    type:'week',
                    count:1,
                    text:'1w'
                },
                {
	           type: 'month',
	           count: 1,
	           text: '1m'
                }, {
	           type: 'month',
	           count: 3,
	           text: '3m'
                }, {
	           type: 'month',
	           count: 6,
	           text: '6m'
                }, {
	           type: 'ytd',
	           text: 'YTD'
                }, {
	           type: 'year',
	           count: 1,
	           text: '1y'
                }, {
	type: 'all',
	text: 'All'
}],
               inputEnabled: false,
                selected: 0
            },

            title : {
                text : text +" Stock Value"
            },
            xAxis: {
                    
                    categories: obj.Positions
                },
            series : [{
                name : text + ' Stock Price',
                data : c,
                type : 'area',
                threshold : null,
                tooltip : {
                    valueDecimals : 2
                },
                fillColor : {
                    linearGradient : {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops : [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                }
            }],
           exporting: {
        buttons: {
            contextButton: {
                enabled: false
            }    
        }
    }
        });
    });
};

function updateFavorCol(){
    if (localStorage.length>0){
        (function theLoop (i) {
  setTimeout(function () {
      //alert(i);
    var Symbol = localStorage.key(i-1);
            //alert("Symbol is " + Symbol);
            var queryString = "?Symbol=" + Symbol;
    $.getJSON('index.php' + queryString, function (data){
               var json_obj = $.parseJSON(data);
                var changeTotal="";
                var lpriceTotal="";
                var marketCapTotal=""                
                var lprice = json_obj.LastPrice.toFixed(2);
                lpriceTotal = "$ "+lprice;
                var change = json_obj.Change.toFixed(2);
                var changeP = json_obj.ChangePercent.toFixed(2);
                if (changeP<0){
               changeTotal= change +" ( " + changeP + "% )";
                    $("#" + Symbol + " td:nth-child(4)").text(changeTotal).css("color", "red");
                    $("#" + Symbol + " td:nth-child(4)").append("<img src='http://cs-server.usc.edu:45678/hw/hw8/images/down.png' style='width:15;height:15'>");
           }
           else if (changeP>0) {
               changeTotal= change +" ( " + changeP + "% )";
                    $("#" + Symbol + " td:nth-child(4)").text(changeTotal).css("color", "green");
               $("#" + Symbol + " td:nth-child(4)").append("<img src='http://cs-server.usc.edu:45678/hw/hw8/images/up.png' style='width:15;height:15'>");
           }
           else {
                changeTotal= change +" ( " + changeP + "% )";
                    $("#" + Symbol + " td:nth-child(4)").text(changeTotal);
               
           }
            
            $("#" + Symbol + " td:nth-child(3)").text(lpriceTotal);
  
            var marketCap = json_obj.MarketCap/1000000000;
           var marketCapR = marketCap.toFixed(2);    
               marketCapTotal = marketCapR + " Billion"; 
            $("#" + Symbol + " td:nth-child(5)").text(marketCapTotal);
            })
    if (--i) {          // If i > 0, keep going
      theLoop(i);       // Call the loop again, and pass it the current value of i
    }
  }, 500);
})(localStorage.length)
    }
             
}
function updateFavor(){
              if(localStorage.length==0){
                  document.getElementById("favorTable").innerHTML="";
                  document.getElementById("favorTable").innerHTML="<tr><th>Symbol</th><th>Company Name </th><th>Stock Price</th><th>Change(Change Percent)</th><th>Market Cap</th><th></th></tr>";
              }
    else {
        for (i = 0; i < localStorage.length; i++)   {
               document.getElementById("favorTable").innerHTML="";
               var Symbol = localStorage.key(i);             
               document.getElementById("favorTable").innerHTML="<tr><th>Symbol</th><th>Company Name </th><th>Stock Price</th><th>Change(Change Percent)</th><th>Market Cap</th><th></th></tr>";
                var queryString = "?Symbol=" + Symbol;
               // localStorage.getItem(localStorage.key(i));
                    //alert(queryString);
                    //alert(i);
   
             // var Symbol = localStorage.getItem(localStorage.key(i));
               var Symbol = localStorage.key(i);
			var queryString = "?Symbol=" + Symbol;
    $.getJSON('index.php' + queryString, function (data){
			 var json_obj = $.parseJSON(data);//parse JSON 
               writeFavor(json_obj);
            })
           }
    }
}
    
            
function requestNewsByFavor(symbol){
    //alert(symbol);
    var queryString = "?News=" + symbol;
    $.getJSON('index.php' + queryString, function (data) { 
        var obj = $.parseJSON(data);
        
        var monthNames = [
  "Jan", "Feb", "Mar",
  "Apr", "May", "June", "July",
  "Aug", "Sep", "October",
  "Nov", "Dec"
];
    
        var length = obj.d.results.length;
        var doc = "";

        
        for (var i=0; i< length;i++){
             var date = new Date(obj.d.results[i].Date);
//alert (date);
            var day = date.getDate();
            if (day<10){
                day = "0"+ day;
            }
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            var hour = date.getHours();
            var min = date.getMinutes();
            var sec = date.getSeconds();
            if (hour<10){
                    hour = "0" + hour;
                }
                var min = date.getMinutes();
                if (min<10){
                    min = "0" + min;
                }
                var sec = date.getSeconds();
                if (sec <10){
                    sec = "0" + sec;
                }
            var time = day + ' ' + monthNames[monthIndex] + ' ' + year + ', ' + hour + ':' + min + ':' + sec;
            doc += "<div class='well well-lg'><a target='_blank' href='" + obj.d.results[i].Url +"'>" + obj.d.results[i].Title + "</a>";
            doc +="<p> </p><br />";
            doc += obj.d.results[i].Description + "<br/><br/>";
            doc += "<b>Pbulisher:" + obj.d.results[i].Source + "</b>";
            doc +="<p> </p><br />";
            doc +="<b>Date:" + time + "</b></div>";
            doc +="<p> </p><br />";
        }
        document.getElementById("thirdresult").innerHTML = doc;
    })
}
function requestNews(){
    
    var text = document.getElementsByName("text")[0].value;
        var queryString = "?News=" + text;
    $.getJSON('index.php' + queryString, function (data) { 
        var obj = $.parseJSON(data);
        var monthNames = [
  "Jan", "Feb", "Mar",
  "Apr", "May", "June", "July",
  "Aug", "Sep", "October",
  "Nov", "Dec"
];


        var length = obj.d.results.length;
        var doc = "";
        //var date = newDate(obj.d.results[i].Date).toLocaleString();
        for (var i=0; i< length;i++){
                var date = new Date(obj.d.results[i].Date);
                //alert (date);
                var day = date.getDate();
                if (day<10){
                    day = "0"+ day;
                }
                var monthIndex = date.getMonth();
                var year = date.getFullYear();
                var hour = date.getHours();
                if (hour<10){
                    hour = "0" + hour;
                }
                var min = date.getMinutes();
                if (min<10){
                    min = "0" + min;
                }
                var sec = date.getSeconds();
                if (sec <10){
                    sec = "0" + sec;
                }
            var time = day + ' ' + monthNames[monthIndex] + ' ' + year + ', ' + hour + ':' + min + ':' + sec;
            doc += "<div class='well well-lg'><a target='_blank' href='" + obj.d.results[i].Url +"'>" + obj.d.results[i].Title + "</a>";
            doc +="<p> </p><br />";
            doc += obj.d.results[i].Description + "<br/><br/>";
            doc += "<b>Pbulisher:" + obj.d.results[i].Source + "</b>";
            doc +="<p> </p><br />";
            doc +="<b>Date:" + time + "</b></div>";
            doc +="<p> </p><br />";
        }
        document.getElementById("thirdresult").innerHTML = doc;
    })
}